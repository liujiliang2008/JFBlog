package com.jfblog.config;

import com.alibaba.druid.filter.stat.MergeStatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfblog.controller.AdminController;
import com.jfblog.controller.FileUploadController;
import com.jfblog.controller.IndexController;
import com.jfblog.model.Article;
import com.jfblog.model.Friendly;
import com.jfblog.model.Message;
import com.jfblog.model.Tags;
import com.jfblog.model.User;
import com.jfblog.utils.PropsUtil;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.FakeStaticHandler;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;

/**
 * 
 * @ClassName: JFBlogConfig
 * @Description: 初始化配置(这里用一句话描述这个类的作用)
 * @author Realfighter
 * @date 2014-12-6 下午04:02:09
 * 
 */
public class JFBlogConfig extends JFinalConfig {

	/**
	 * 配置常量
	 */
	@Override
	public void configConstant(Constants me) {
		// loadPropertyFile("jdbc-config.txt");
		// 设置开发模式
		me.setDevMode(Boolean.parseBoolean(getProperty("devMode")));

		me.setBaseViewPath("/WEB-INF/pages");

		// 设置视图层使用JSP
		me.setViewType(ViewType.JSP);
	}

	/**
	 * 配置处理器
	 */
	@Override
	public void configHandler(Handlers me) {
		me.add(new FakeStaticHandler());
	}

	/**
	 * 配置全局拦截器
	 */
	@Override
	public void configInterceptor(Interceptors me) {

	}

	/**
	 * 配置插件
	 */
	@Override
	public void configPlugin(Plugins me) {

		// 配置缓存插件
		EhCachePlugin ehCachePlugin = new EhCachePlugin();
		me.add(ehCachePlugin);

		// 增加Druid连接池插件配置
		DruidPlugin druidPlugin = new DruidPlugin(getProperty("jdbcUrl"), getProperty("user"), getProperty("password")
				.trim());
		druidPlugin.addFilter(new MergeStatFilter());
		druidPlugin.setMaxActive(150);
		WallFilter wall = new WallFilter();
		wall.setDbType(getProperty("dbType"));
		druidPlugin.addFilter(wall);
		me.add(druidPlugin);

		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		me.add(arp);

		// 添加映射关系
		arp.addMapping("user", User.class);
		arp.addMapping("message", Message.class);
		arp.addMapping("tags", Tags.class);
		arp.addMapping("article", Article.class);
		arp.addMapping("friendly", Friendly.class);
	}

	/**
	 * 配置路由
	 */
	@Override
	public void configRoute(Routes me) {
		me.add("/", IndexController.class, "/");
		me.add("/admin", AdminController.class, "/admin");
		me.add("/upload", FileUploadController.class, "/");
	}

	/**
	 * 重写配置文件读取方法
	 */
	@Override
	public String getProperty(String key) {
		return PropsUtil.get(key);
	}

}
