package com.jfblog.bean;

/**
 * 
 * @ClassName: Constants
 * @Description: 系统常量(这里用一句话描述这个类的作用)
 * @author Realfighter
 * @date 2014-12-6 下午05:15:57
 * 
 */
public class Constants {

	public static final int ISADMIN = 1;

	public static final String ALL = "ALL";// 后台标签查询全部

	public static final int Realfighter = -1;// 关于Realfighter的标识
	
	public static final int message = -2;// 留言

	public static final int archive = -3;// 归档

	public static final int cates = -4;// category分类

}
