package com.jfblog.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

/**
 * 
 * @ClassName: Friendly
 * @Description: 友情链接(这里用一句话描述这个类的作用)
 * @author Realfighter
 * @date 2014-12-16 下午06:00:59
 *
 */
@SuppressWarnings("serial")
public class Friendly extends Model<Friendly> {

	public static final Friendly dao = new Friendly();

	public List<Friendly> findAll() {
		return find("select * from friendly order by orders");
	}

	public Page<Friendly> findFriendlys4Page(int pagenum, int pageSize) {
		return paginate(pagenum, pageSize, "select * ", "from friendly");
	}

}
