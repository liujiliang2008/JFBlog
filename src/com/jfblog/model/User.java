package com.jfblog.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * 用户信息
 * 
 * @author Realfighter
 * 
 */
@SuppressWarnings("serial")
public class User extends Model<User> {

	public static final User dao = new User();

	public User findByName(String name) {
		return findFirst("select * from user where name=?", name);
	}

	public User findLoginUser(String username, String password) {
		return findFirst("select * from user where upper(name)=? and pwd=?",
				new Object[] { username.toUpperCase(), password });
	}

}
