package com.jfblog.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jfblog.bean.LuceneBean;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

/**
 * 文章
 */
@SuppressWarnings("serial")
public class Article extends Model<Article> {

	private static final Logger logger = Logger.getLogger(Article.class);

	public static final Article dao = new Article();

	public void deleteByTagid(int tagid) {
		List<Article> articles = find("select id from article where tagsid=?",
				tagid);
		for (Article article : articles) {
			deleteById(article.getInt("id"));
		}
	}

	public List<Article> findAllArticleImages() {
		String sql = "select distinct picture from article where id>0 and isdraft=0";
		return find(sql);
	}

	/**
	 * 分页查询文章信息
	 * 
	 * @Title: findArticles4Page
	 * @author Realfighter
	 * @param pagenum
	 * @param pageSize
	 * @param tagid
	 *            分类标签ID
	 * @param time
	 *            月份归档查询
	 * @param userid
	 *            用户ID,后台管理只查询当前用户的文章,前台查询所有的正常文章
	 * @param isadmin
	 *            是否后台管理员
	 * @return Page<Article>
	 * @throws
	 */
	public Page<Article> findArticles4Page(int pagenum, int pageSize,
			long tagid, String time, int userid, boolean isadmin) {
		String exceptString = "from article t1,tags t2,user t3 where t1.tagsid=t2.id and t1.userid=t3.id";
		if (tagid > 0) {
			exceptString += " and t2.id=" + tagid;
		}
		if (!time.equals("0-0")) {
			exceptString += " and date_format(t1.createtime,'%Y-%m') = '"
					+ time + "'";
		}
		exceptString += (isadmin ? (" and t1.userid=" + userid)
				: " and t1.isdraft=0")
				+ " order by t1.createtime desc";
		Page<Article> page = paginate(pagenum, pageSize,
				"select t1.*,t2.name,t3.name username ", exceptString);
		return page;
	}

	/**
	 * 首页归档功能查询
	 * 
	 * @Title: findAllArchiveTimes
	 * @author Realfighter
	 * @return List<Article>
	 * @throws
	 */
	public List<Article> findAllArchiveTimes() {
		String sql = "select distinct(date_format(createtime,'%Y-%m')) createtime from article where isdraft=0 group by date_format(createtime,'%Y-%m') order by createtime desc";
		return find(sql);
	}

	public List<LuceneBean> findOnline() {
		String sql = "select id,title,remark from article where id>0 and isdraft=0";
		List<Article> articles = find(sql);
		List<LuceneBean> beans = new ArrayList<LuceneBean>();
		for (Article article : articles) {
			LuceneBean bean = new LuceneBean(article.getInt("id"), article
					.getStr("title"), article.getStr("remark"));
			beans.add(bean);
		}
		return beans;
	}

	/**
	 * 通过关键字查询
	 * 
	 * @param pagenum
	 * @param pageSize
	 * @param searchKey
	 * @param aids
	 * @return
	 */
	public Page<Article> findArticle4PageByKey(int pagenum, int pageSize,
			String searchKey, String aids) {
		String exceptString = "from article t1,tags t2,user t3 where t1.tagsid=t2.id and t1.userid=t3.id";
		if ("".equals(aids)) {
			exceptString += " and (t1.title like '%" + searchKey
					+ "%' or t1.remark like '%" + searchKey + "%')";
			logger.info("当前关键字【" + searchKey + "】无法通过lucene检索，操作数据库再次检索。");
		} else {
			exceptString += " and t1.id in (" + aids + ")";
			logger.info("当前关键字【" + searchKey + "】通过lucene检索成功！");
		}
		exceptString += " and t1.isdraft=0 order by t1.createtime desc";
		Page<Article> page = paginate(pagenum, pageSize,
				"select t1.*,t2.name,t3.name username ", exceptString);
		return page;
	}

	/**
	 * 文章归档查询
	 * 
	 * @return
	 */
	public List<Article> findArchives() {
		String sql = "select id,title,createtime from article where id>0 and isdraft=0 order by createtime desc";
		return find(sql);
	}

	/**
	 * 查询上一篇下一篇
	 * 
	 * @param articleId
	 * @return
	 */
	public List<Article> findArticlePrevAndNextById(int articleId) {
		String select = "(select id fid,title ftitle from article where id < ? and isdraft=0 order by id desc limit 1)"
				+ " union (select id fid,title ftitle from article where id > ? and isdraft=0 order by id asc limit 1)";
		List<Article> articles = find(select, new Object[] { articleId,
				articleId });
		return articles;
	}

	/**
	 * 查询文章详情
	 * 
	 * @param type
	 * @return
	 */
	public Article findArticleInfoById(int type) {
		String select = "select t1.*,case when t1.tagsid<=0 then t1.tagsid else (select name from tags where id=t1.tagsid) end tagname,t2.name username from article t1,user t2 where t1.userid=t2.id and t1.id=?";
		Article article = findFirst(select, type);
		return article;
	}
}
