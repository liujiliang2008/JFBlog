package com.jfblog.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;

/**
 * LoginInterceptor.
 */
public class LoginInterceptor implements Interceptor {

	public void intercept(ActionInvocation ai) {
		Integer userid = ai.getController().getSessionAttr("xx566_userid");
		if (userid == null) {
			ai.getController().redirect("/admin/login.html");
		} else {
			ai.invoke();
		}

	}

}
