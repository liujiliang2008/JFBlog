package com.jfblog.controller;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jfblog.bean.CacheBean;
import com.jfblog.bean.Constants;
import com.jfblog.bean.LuceneBean;
import com.jfblog.common.JFBlogCache;
import com.jfblog.common.JFBlogCache.CacheKey;
import com.jfblog.interceptor.LoginInterceptor;
import com.jfblog.model.Article;
import com.jfblog.model.Friendly;
import com.jfblog.model.Message;
import com.jfblog.model.Tags;
import com.jfblog.model.User;
import com.jfblog.utils.LuceneUtil;
import com.jfblog.utils.PageUtil;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheKit;

/**
 * admin相关处理Controller
 * 
 * @author Realfighter
 * 
 */
public class AdminController extends Controller {

	private static final Logger logger = Logger
			.getLogger(AdminController.class);

	/**
	 * 登录页面
	 */
	public void login() {
		render("login.jsp");
	}

	/**
	 * 登录表单提交
	 */
	public void loginToIndex() {
		String username = getPara("name");
		String password = getPara("password");
		String returnUrl = getPara("returnUrl");
		setAttr("returnUrl", returnUrl);
		User user = User.dao.findLoginUser(username, password);
		if (user == null) {
			setAttr("error_info", "用户名或邮箱地址错误");
			render("error.jsp");
		} else if (user.getInt("state") == Constants.ISADMIN) {// 是否可以登录后台
			setSessionAttr("xx566_userid", user.getInt("id"));
			setSessionAttr("isadmin", true);
			redirect("/admin/artList.html");
		} else {
			setSessionAttr("xx566_userid", user.getInt("id"));
			redirect("/");
		}
	}

	/**
	 * 文章管理列表
	 */
	@Before(LoginInterceptor.class)
	public void artList() {
		// 获取用户ID
		Integer userid = getSessionAttr("xx566_userid");
		int pagenum = 1;
		int pageSize = 20;
		long tagid = 0;// 标签ID
		String time = "0-0";// 默认日期
		if (!isParaBlank(0)) {// 页码
			pagenum = getParaToInt(0);
		}
		Page<Article> page = Article.dao.findArticles4Page(pagenum, pageSize,
				tagid, time, userid, true);
		setAttr("page", new PageUtil(page));
		render("artlist.jsp");
	}

	/**
	 * 留言列表
	 * 
	 * @Title: msgList
	 * @author Realfighter void
	 * @throws
	 */
	@Before(LoginInterceptor.class)
	public void msgList() {
		int pagenum = 1;
		int pageSize = 10;
		if (!isParaBlank(0)) {// 页码
			pagenum = getParaToInt(0);
		}
		Page<Message> page = Message.dao.findMsg4Page(pagenum, pageSize);
		setAttr("page", new PageUtil(page));
		render("msglist.jsp");
	}

	/**
	 * 删除留言
	 * 
	 * @Title: msgdelete
	 * @author Realfighter void
	 * @throws
	 */
	@Before(LoginInterceptor.class)
	public void msgdelete() {
		final int msgid = getParaToInt(0);
		boolean succeed = Db.tx(new IAtom() {

			public boolean run() throws SQLException {
				try {
					// 删除留言
					Message.dao.deleteById(msgid);
					// 删除留言下所有回复
					Message.dao.deleteByMsgid(msgid);
					return true;
				} catch (Exception e) {
					e.printStackTrace();
				}
				return false;
			}
		});
		if (succeed) {
			// 更新缓存
			CacheKit.put(CacheBean.MESSAGES, CacheBean.MESSAGES, Message.dao
					.findMsg4Page(1, 10));
			renderText("操作成功");
		} else {
			renderText("网络异常");
		}
	}

	/**
	 * 跳转到发布页面
	 */
	@Before(LoginInterceptor.class)
	public void toPublish() {
		if (!isParaBlank(0)) {// 文章ID不为空表示编辑
			setAttr("adminArtID", getParaToInt());
		}
		render("publish.jsp");
	}

	/**
	 * 获取左侧标签树
	 */
	@Before(LoginInterceptor.class)
	public void lefting() {
		if (!isParaBlank(0) && getParaToInt() > 0) {// 存在文章ID
			setAttr("tagid", Article.dao.findById(getParaToInt()).getInt(
					"tagsid"));
		}
		List<Tags> tags = CacheKit.get(CacheBean.TAGS, CacheBean.TAGS,
				JFBlogCache.getIDataLoaderByKey(CacheKey.tags));
		Map<String, String> map = new HashMap<String, String>();
		List<String> list = new ArrayList<String>();
		int count = 0;
		for (Tags tag : tags) {
			String str = "";
			if (!map.containsKey(tag.getStr("type"))) {
				count--;
				str += "{id:" + count + ",";
				str += "pId:0,";
				str += "name:'" + tag.getStr("type") + "'}";
				map.put(tag.getStr("type"), String.valueOf(count));
				list.add(str);
			}
			str = "{id:" + tag.getLong("id") + ",";
			str += "pId:" + map.get(tag.getStr("type")) + ",";
			str += "name:'" + tag.getStr("name") + "'}";
			list.add(str);
		}
		setAttr("admin_tags", list.toString());
		render("include/left.jsp");
	}

	/**
	 * main页面
	 */
	@Before(LoginInterceptor.class)
	public void centering() {
		if (!isParaBlank(0) && getParaToInt() > 0) {// 存在文章ID
			setAttr("article_admin", Article.dao.findById(getParaToInt()));
		}
		List<String> list = new ArrayList<String>();
		// 获取upload/index下面所有的文件名
		String folder = "upload/index";
		File file = new File(PathKit.getWebRootPath() + File.separator + folder);
		String[] files = file.list();
		if (files != null) {
			for (String filename : files) {
				list.add(folder + "/" + filename);
			}
		}
		// 这里不再获取所有上传的图片，修改为获取文章的所有配图
		// List<Article> files = Article.dao.findAllArticleImages();
		// for (Article article : files) {
		// list.add(article.getStr("picture"));
		// }
		setAttr("uploadlist", list);
		render("include/center.jsp");
	}

	/**
	 * header
	 */
	@Before(LoginInterceptor.class)
	public void toping() {
		render("include/top.jsp");
	}

	/**
	 * 标签管理
	 */
	@Before(LoginInterceptor.class)
	public void tagList() {
		int pagenum = 1;
		int pageSize = 20;
		String category = Constants.ALL;// 默认分类
		if (!isParaBlank(0)) {// 页码
			pagenum = Math.max(getParaToInt(0), pagenum);
		}
		Page<Tags> page = null;
		if (!isParaBlank(1) && !getPara(1).equals(category)) {
			try {
				category = URLDecoder.decode(getPara(1), "utf-8");
			} catch (UnsupportedEncodingException e) {
				// 中文转码异常
				e.printStackTrace();
			}
		}
		List<Tags> cateList = Tags.dao.findCateList();
		setAttr("tags_cateList", cateList);
		setAttr("tag_category", category);
		page = Tags.dao.findTags4Page(pagenum, pageSize, category);
		setAttr("page", new PageUtil(page));
		render("taglist.jsp");
	}

	/**
	 * 新增或编辑标签，通过tagid为0区分
	 */
	@Before(LoginInterceptor.class)
	public void tagedit() {
		List<Tags> cateList = Tags.dao.findCateList();
		setAttr("tags_cateList", cateList);
		if (!isParaBlank(0)) {
			int tagid = getParaToInt(0);
			Tags tags = Tags.dao.findById(tagid);
			setAttr("tags_tags", tags);
		}
		render("tagsedit.jsp");
	}

	/**
	 * 标签保存
	 */
	@Before(LoginInterceptor.class)
	public void savetagedit() {
		Tags tags = getModel(Tags.class);
		if (tags.getLong("id") == null || tags.getLong("id") == 0) {
			tags.save();
		} else {
			tags.update();
		}
		// 更新缓存
		CacheKit.put(CacheBean.TAGS, CacheBean.TAGS, Tags.dao.findAll());
		CacheKit.put(CacheBean.CATES, CacheBean.CATES, Tags.dao.findCateList());
		renderText("操作成功");
	}

	/**
	 * 删除标签
	 */
	@Before(LoginInterceptor.class)
	public void tagdelete() {
		final int tagid = getParaToInt(0);
		boolean succeed = Db.tx(new IAtom() {
			public boolean run() throws SQLException {
				try {
					// 删除标签
					Tags.dao.deleteById(tagid);
					// 删除标签下所有文章
					Article.dao.deleteByTagid(tagid);
					return true;
				} catch (Exception e) {
					// 标签删除异常
					e.printStackTrace();
				}
				return false;
			}
		});
		if (succeed) {
			// 更新缓存
			CacheKit.put(CacheBean.TAGS, CacheBean.TAGS, Tags.dao.findAll());
			CacheKit.put(CacheBean.CATES, CacheBean.CATES, Tags.dao
					.findCateList());
			renderText("操作成功");
		} else {
			renderText("网络异常");
		}
	}

	/**
	 * 关于Realfighter功能
	 */
	@Before(LoginInterceptor.class)
	public void about() {
		Article realfighter = Article.dao.findById(Constants.Realfighter);
		setAttr("realfighter", realfighter);
		render("about.jsp");
	}

	/**
	 * 关于功能提交
	 */
	@Before(LoginInterceptor.class)
	public void aboutPublish() {
		Article article = getModel(Article.class);
		Article.dao.deleteById(Constants.Realfighter);
		article.save();
		redirect("/admin/about.html");
	}

	/**
	 * 标签管理
	 */
	@Before(LoginInterceptor.class)
	public void linkList() {
		int pagenum = 1;
		int pageSize = 20;
		if (!isParaBlank(0)) {// 页码
			pagenum = getParaToInt(0);
		}
		Page<Friendly> page = Friendly.dao
				.findFriendlys4Page(pagenum, pageSize);
		setAttr("page", new PageUtil(page));
		render("linklist.jsp");
	}

	/**
	 * 跳转到编辑或保存友链
	 */
	@Before(LoginInterceptor.class)
	public void linksedit() {
		if (!isParaBlank(0)) {
			int linksid = getParaToInt(0);
			Friendly friendly = Friendly.dao.findById(linksid);
			setAttr("friendly_friendly", friendly);
		}
		render("linksedit.jsp");
	}

	/**
	 * 编辑或保存友链
	 */
	@Before(LoginInterceptor.class)
	public void savelinksedit() {
		Friendly friendly = getModel(Friendly.class);
		if (friendly.getInt("id") == null || friendly.getInt("id") == 0) {
			friendly.save();
		} else {
			friendly.update();
		}
		// 更新缓存
		CacheKit.put(CacheBean.FRIENDLYS, CacheBean.FRIENDLYS, Friendly.dao
				.findAll());
		renderText("操作成功");
	}

	/**
	 * 删除友链
	 */
	@Before(LoginInterceptor.class)
	public void linksdelete() {
		// 删除标签
		Friendly.dao.deleteById(getParaToInt(0));
		// 更新缓存
		CacheKit.put(CacheBean.FRIENDLYS, CacheBean.FRIENDLYS, Friendly.dao
				.findAll());
		renderText("操作成功");
	}

	/**
	 * 发表文章
	 */
	@Before(LoginInterceptor.class)
	public void publish() {
		Article article = getModel(Article.class);
		// 获取用户ID
		Integer userid = getSessionAttr("xx566_userid");
		article.set("userid", userid);
		LuceneBean luceneBean = null;
		if (article.getInt("id") != null) {// 标识编辑文章
			article.update();
			// 更新索引
			luceneBean = new LuceneBean(article.getInt("id"), article
					.getStr("title"), article.getStr("remark"));
			try {
				LuceneUtil.updateIndex(luceneBean);
			} catch (Exception e) {
				logger.error("更新文章索引异常!" + e);
			}
		} else {
			article.save();// 保存
			// 添加索引
			luceneBean = new LuceneBean(article.getInt("id"), article
					.getStr("title"), article.getStr("remark"));
			try {
				LuceneUtil.addIndex(luceneBean);
			} catch (Exception e) {
				logger.error("添加文章索引异常!" + e);
			}
		}
		// 首页文章缓存更新
		CacheKit.put(CacheBean.ARTICLES, CacheBean.ARTICLES, Article.dao
				.findArticles4Page(1, 10, 0, "0-0", userid, false));
		// 归档信息缓存
		CacheKit.put(CacheBean.ARCHIVE_TIMES, CacheBean.ARCHIVE_TIMES,
				Article.dao.findAllArchiveTimes());
		redirect("/admin/artList.html");// 返回文章管理列表
	}

	/**
	 * 创建文章索引（只生成一次）
	 */
	@Before(LoginInterceptor.class)
	public void generateIndexes() {
		String result = "创建文章索引成功";
		List<LuceneBean> articles = Article.dao.findOnline();
		try {
			LuceneUtil.createIndex(articles);
		} catch (Exception e) {
			result = "创建文章索引异常";
			logger.error("创建文章索引异常!" + e);
		}
		renderText(result);
	}

	/**
	 * 删除
	 * 
	 * @Title: artdel
	 * @author Realfighter
	 */
	@Before(LoginInterceptor.class)
	public void artdel() {
		int artid = 0;
		if (!isParaBlank(0)) {// 文章ID
			artid = getParaToInt(0);
		}
		// 获取用户ID
		Integer userid = getSessionAttr("xx566_userid");
		User user = User.dao.findFirst("select * from user where id=?", userid);
		String renderString = "网络异常";
		// 校验是否admin用户
		if (user.getInt("state") == 1) {
			if (artid > 0) {// 删除
				Article.dao.findById(artid).delete();
				// 更新缓存
				CacheKit.put(CacheBean.ARTICLES, CacheBean.ARTICLES,
						Article.dao.findArticles4Page(1, 10, 0, "0-0", userid,
								false));
				CacheKit.put(CacheBean.ARCHIVE_TIMES, CacheBean.ARCHIVE_TIMES,
						Article.dao.findAllArchiveTimes());
				renderString = "操作成功";
				// 删除索引
				LuceneBean vo = new LuceneBean(artid, null, null);
				try {
					LuceneUtil.deleteIndex(vo);
				} catch (Exception e) {
					renderString = "操作成功，但删除索引异常";
					logger.error("删除文章索引异常!" + e);
				}
			}
		}
		renderText(renderString);
	}

}
