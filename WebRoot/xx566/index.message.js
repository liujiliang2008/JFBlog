define( [ "jquery"], function($) {
	var message = {};
	$("#msgpid").val(0);
	//发布留言
	message.addMsg = function(){
		$("#response_message span[id]").html("");
//		$("#msg_form span").html("");
		var msgForm = $(this).parent().parent();
		$(msgForm).find("#returnUrl").val(window.location.href);
		if(xx566_userid==''){
			var name = $(msgForm).find("#username").val();
			var pwd = $(msgForm).find("#useremail").val();
			if (name == '') {
				$(msgForm).find("#nameMsg").html("请输入用户名");
				return;
			}
			if (!/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(pwd)) {
				$(msgForm).find("#emailMsg").html("请输入正确的邮箱地址");
				return;
			}
			$.post("isNameExits.html", { name: name },function(data){
				if(data=='true'){
					$(msgForm).find("#nameMsg").html("当前用户名已存在");
					return;
				}else{
					$(msgForm).submit();
				}
			});
		}else{
			$(msgForm).submit();
		}
	}
	//留言form跟随
	message.followMsg = function(){
		$("article[rel^='msg_reback']").hide();
		$("#response_message").hide();
		$("#response_message  div:hidden").show();
		$("#response_message span[id]").html("");
		var hml = $("#response_message").html();
		$(this).parent().after(hml);
		$(this).parent().next().show();
		$(this).parent().next().find("#msgpid").val($(this).attr("msgpid"));
	}
	
	//留言form取消跟随
	message.cancleMsg = function(){
		$("#response_message article[rel^='msg_reback']").show();
		$("#response_message").show();
		$("#response_message  .cancel-comment").hide();
		$(this).parent().parent().remove();
	}
	return message;
});