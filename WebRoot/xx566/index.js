define( [ "jquery" ], function($) {
	var index = {};
	index.search = function() {
		if ($("#search_key").val() == '') {
			return false;
		}
		$("#search_form").attr("action",
				"1-0-0-0-" + encodeURI(encodeURI($("#search_key").val()))+".html");
		$("#search_form").submit();
	}
	index.category = function() {
		var cate = $(this).attr("ref");
		$(this).attr("href", "cates/n4-1-" + encodeURI(encodeURI(cate)));
	}
	return index;
});