define( [ "jquery", "jquery.artDialog" ], function($) {
	var message = {};
	
	//用户信息
	message.userinfo = function(){
		$("#msg_username").html($(this).attr("name"));
		$("#msg_website").html($(this).attr("web"));
		$("#msg_userip").html($(this).attr("userip"));
		$("#msg_email").html($(this).attr("email"));
		$("#msg_website").attr("href",$(this).attr("web"));
		$("#msg_pic").attr("src",$(this).attr("pic"));
		$.dialog({
    		lock: true,
    		background: '#000000', 
		    opacity: 0.5,
		    width: 400,
		    height: 230,
		    fixed: true,
			drag: false,
    		resize: false,
			title: '用户信息',
			content: document.getElementById("msgs_dialog")
		});
	}
	
	//删除留言
	message.remove = function(){
		var msgid = $(this).attr("msgid");
		$.dialog({
    		lock: true,
    		background: '#000000', 
		    opacity: 0.5,
		    fixed: true,
			drag: false,
    		resize: false,
			title: '消息提示',
			content:'删除操作会同步删除此留言下所有回复，确定删除吗？',
			button: [
		        {
		            name: '确定',
		            callback:function(){
		        		$.ajax({
						   type: "POST",
						   url: "admin/msgdelete/"+msgid,
						   success: function(msg){
						   	  window.location.href = window.location.href;
						   }
						});
		            }
		        },
		        {
		            name: '取消'
		        }
   			 ]
		});
	}
	
	return message;
});