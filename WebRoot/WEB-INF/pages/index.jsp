<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<%@include file="/common/common_meta.jsp" %>
		<link href="css/grid.css" rel="stylesheet" type="text/css" />
		<link href="css/icon.css" rel="stylesheet" type="text/css" />
		<link href="css/style.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<%@include file="/common/header.jsp" %>
		<article class="container content">
			<article class="row">
				<section class="article-list col-tb-8 col-mb-12">
					<c:if test="${page.rowCount!=0&&(index_tagid!=0||index_time!='0-0')&&page.navCount>=page.num}">
						<section id="breadcrumb">
					        <a href="">首页</a>
					        <span>&gt;</span>
					        <c:if test="${index_tagid!=0}">
					        ${page.list[0].name}
		            		</c:if>
		            		<c:if test="${index_time!='0-0'}">
		            		${fn:split(index_time,'-')[0]}年${fn:split(index_time,'-')[1]}月
	            		 	</c:if>
		            	</section>
	            	</c:if>
					<%@include file="list_blog.jsp" %>
				</section>
				<section class="side-list col-tb-4 col-mb-12">
<%--					标签云--%>
<%--					<%@include file="tags_cloud.jsp" %>--%>
					<%--最新文章--%>
					<%@include file="latest_blog.jsp" %>
					<%--最新回复--%>
					<%@include file="latest_reply.jsp" %>
					<%--分类标签--%>
					<%@include file="category_tags.jsp" %>
					<%--友情链接--%>
					<%@include file="friendly_links.jsp" %>
				</section>
			</article>
		</article>
  		<%@include file="/common/footer.jsp" %>
  		<script src="js/require.js" defer async="true" data-main="js/index"></script>
	</body>
</html>