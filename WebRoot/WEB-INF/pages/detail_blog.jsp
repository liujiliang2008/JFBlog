<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<article>

     <h2 class="article-title">
         <a style="cursor: hand;cursor: pointer;">${article.title}</a>
     </h2>

     <ul class="article-meta">
         <li><fmt:formatDate value="${article.createtime}" pattern="yyyy/MM/dd" /></li>
         <li><span>|</span></li><li><a href="1-${article.tagsid}-0-0.html">${article.tagname}</a></li>
         <li><span>|</span></li>
         <li>
             <a rel="external nofollow"> ${article.clicks} Views </a>
          </li>
         <li><span>|</span></li>
         <li>author：<a onclick="return false;" style="cursor: pointer;">${article.username}</a></li>
     </ul>

	<section class="article-content clearfix">
        ${article.content}
	</section>
	<section class="article-copyright">
        <c:forEach items="${article2}" var="arts">
            <c:if test="${arts.fid >= 0 && arts.fid < article.id}">
                <p>&lt;&lt;上一篇：<a href="detail/${arts.fid}.html">${arts.ftitle}</a></p>
            </c:if>
            <c:if test="${arts.fid > article.id}">
                <p>&gt;&gt;下一篇：<a href="detail/${arts.fid}.html">${arts.ftitle}</a></p>
            </c:if>
        </c:forEach>
        <p>转载注明：<a href="">Realfighter</a>&gt;&gt;<a href="">${article.title}</a></p>
        <p>本文链接：<a href="" id="detail_url"><script type="text/javascript">document.getElementById("detail_url").href=window.location.href;document.write(window.location.href);</script></a></p>
    </section>

    <%@include file="message_form.jsp" %>

</article>