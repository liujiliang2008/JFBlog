<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<section>
	<div id="response_message">
	   	<article class="new-comment" rel="msg_reback">
	       	<div class="cancel-comment" style="display:none;">
	        	<a rel="nofollow" style="cursor: hand;cursor: pointer;" id="cancel-comment-reply-link">取消回复</a>
	        </div>
	    	<form action="save.html" method="post" id="msg_form">
	    		<input type="hidden" value="<c:if test='${articletag=="-3"}'>-3</c:if><c:if test='${articletag=="-4"}'>-4</c:if><c:if test='${articletag!="-3"&&articletag!="-4"}'>${article.id}</c:if>" name="message.articleid"/>
	    		<input type="hidden" value="0" name="message.pid" id="msgpid"/>
	    		<input type="hidden" value="" name="returnUrl" id="returnUrl"/>
	    		<c:if test="${sessionScope.xx566_userid==null}">
					<p><input type="text" value="${user.name}" name="user.name" id="username"><label for="author">姓名</label><span id="nameMsg"></span></p>
			      	<p><input type="email" value="${user.email}" name="user.email" id="useremail"><label for="mail">邮箱</label><span id="emailMsg"></span></p>
			       	<p><input type="url" value="${user.website}" name="user.website" placeholder="http://" value="http://" onfocus="javascript:if($(this).val()=='http://'){$(this).val('')}" onblur="javascript:if($(this).val()==''){$(this).val('http://')}" style="color:#6d6d6d;"><label for="url">网站</label></p>
		        </c:if>
		        <textarea name="message.content" value="${message.content}" rows="4" placeholder="大雁从天空飞过，怎能不留下痕迹？" onfocus="javascript:if($(this).html()=='大雁从天空飞过，怎能不留下痕迹？'){$(this).html('')}" onblur="javascript:if($(this).html()==''){$(this).html('大雁从天空飞过，怎能不留下痕迹？')}" style="color:#6d6d6d;">大雁从天空飞过，怎能不留下痕迹？</textarea>
		        <div class="clearfix">
		        	<c:if test="${sessionScope.xx566_userid==null}">
		        		<span style="color: #777777;">您也可以通过姓名和邮箱</span><a style="cursor: pointer;color: #5895BE;" href="admin/login.html">登录</a><span style="color: #777777;">，快速发布评论</span>
		        	</c:if>
		        	<button type="button" id="msg_submit">发布评论</button>
		        </div>
	       	</form>
	   	</article>
   	</div>
   	
   	<article class="comments-list">
       	<ol class="comment-list">
       	<c:forEach items="${msgPage.list}" var="msgs">
       		<li class="comment-body comment-parent comment-odd" itemtype="" itemscope="">
        		<div itemtype="" itemscope="" itemprop="creator" class="comment-author">
			        <span itemprop="image"><img width="32" height="32" src="${msgs.avatar}" class="avatar"></span>
			        <cite itemprop="name" class="fn">
			        	<c:if test="${msgs.website!=''}">
			        		<a rel="external nofollow" href="${msgs.website}">${msgs.name}</a>
			        	</c:if>
			        	<c:if test="${msgs.website==''}">${msgs.name}</c:if>
			        </cite>
			    </div>
			    <div class="comment-meta">
		        	<fmt:formatDate value="${msgs.createtime}" pattern="yyyy/MM/dd, HH:mm:ss" />
	            </div>
	            <div itemprop="commentText" class="comment-content">
			    	<p>${msgs.content}</p>
			    </div>
			    <div class="comment-reply">
       				<a rel="follow" style="cursor: hand;cursor: pointer;" msgpid="${msgs.id}">回复</a>
       			</div>
       			<c:set value="${msgs.id}" var="msgid"></c:set>
       			<div class="comment-children" itemprop="discusses">
       				<ol class="comment-list">
       				<c:forEach items="${msgMap}" var="msjs">
       					<c:if test="${msgs.id eq msjs.key}">
       						<c:forEach items="${msjs.value}" var="mss">
		       					<li id="comment-395" class="comment-body comment-child comment-level-odd comment-odd comment-by-author" itemtype="" itemscope="">
		       						<div itemtype="" itemscope="" itemprop="creator" class="comment-author">
								        <span itemprop="image"><img width="32" height="32" src="${mss.avatar}" class="avatar"></span>
								        <cite itemprop="name" class="fn">
								        	<c:if test="${mss.website!=''}">
								        		<a rel="external nofollow" href="${mss.website}">${mss.name}</a>
								        	</c:if>
								        	<c:if test="${mss.website==''}">${mss.name}</c:if>
								        </cite>
								    </div>
								    <div class="comment-meta">
						        		<fmt:formatDate value="${mss.createtime}" pattern="yyyy/MM/dd, HH:mm:ss" />
						            </div>
						            <div itemprop="commentText" class="comment-content">
								    	<p>${mss.content}</p>
								    </div>
								    <div class="comment-reply">
		       							<a rel="follow" style="cursor: hand;cursor: pointer;" msgpid="${msgs.id}">回复</a>
		       						</div>
		       					</li>
       						</c:forEach>
       					</c:if>
       				</c:forEach>
       				</ol>
       			</div>
       		</li>
       	</c:forEach>
		</ol>
		<c:if test="${!empty msgPage.list}">
			<ol class="page-navigator">
				<c:if test="${msgPage.num!=1}">
					<li class="prev"><a href="detail/
					<c:if test='${articletag=="-2"}'>n2</c:if>
					<c:if test='${articletag=="-3"}'>n3</c:if>
					<c:if test='${articletag=="-4"}'>n4</c:if>
					<c:if test='${articletag!="-2"&&articletag!="-3"&&articletag!="-4"}'>${article.id}</c:if>.html">首页</a></li>
					<li class="prev"><a href="detail/
					<c:if test='${articletag=="-2"}'>n2</c:if>
					<c:if test='${articletag=="-3"}'>n3</c:if>
					<c:if test='${articletag=="-4"}'>n4</c:if>
					<c:if test='${articletag!="-2"&&articletag!="-3"&&articletag!="-4"}'>${article.id}</c:if>
					-${msgPage.num-1}-${cate_para}.html">上一页</a></li>
				</c:if>
				<c:forEach var="i" begin="${msgPage.begin}" end="${msgPage.end}">
					<li <c:if test="${msgPage.num==i}">style="font-size: 18px;"</c:if>>
					<a href="detail/
					<c:if test='${articletag=="-2"}'>n2</c:if>
					<c:if test='${articletag=="-3"}'>n3</c:if>
					<c:if test='${articletag=="-4"}'>n4</c:if>
					<c:if test='${articletag!="-2"&&articletag!="-3"&&articletag!="-4"}'>${article.id}</c:if>
					-${i}-${cate_para}.html">${i}</a></li>
				</c:forEach>
				<c:if test="${msgPage.navCount!=0&&msgPage.num!=msgPage.last}">
					<li class="next"><a href="detail/
					<c:if test='${articletag=="-2"}'>n2</c:if>
					<c:if test='${articletag=="-3"}'>n3</c:if>
					<c:if test='${articletag=="-4"}'>n4</c:if>
					<c:if test='${articletag!="-2"&&articletag!="-3"&&articletag!="-4"}'>${article.id}</c:if>
					-${msgPage.num+1}-${cate_para}.html">下一页</a></li>
					<li class="next"><a href="detail/
					<c:if test='${articletag=="-2"}'>n2</c:if>
					<c:if test='${articletag=="-3"}'>n3</c:if>
					<c:if test='${articletag=="-4"}'>n4</c:if>
					<c:if test='${articletag!="-2"&&articletag!="-3"&&articletag!="-4"}'>${article.id}</c:if>
					-${msgPage.navCount}-${cate_para}.html">尾页</a></li>
				</c:if>
			</ol>
		</c:if>
	</article>
	<script src="js/require.js" defer async="true" data-main="js/index_message"></script>
</section>