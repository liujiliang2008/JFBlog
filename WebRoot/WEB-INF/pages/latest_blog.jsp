<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<article class="recent-posts">
  	<h3>最近文章</h3>
    <ul>
		<c:forEach items="${articles.list}" var="art">
	       <li><a class="item" href="detail/${art.id}.html">${art.title}</a></li>
		</c:forEach>
	</ul>
</article>