<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<article class="recent-comments">
	<h3>最近回复</h3>
	<ul>
		<c:forEach items="${messages.list}" var="msg">
			<li>
				<a rel="external nofollow" href="${msg.website}">${msg.name}</a> :
	            <a href="detail/<c:if test="${msg.articleid<0}">n${msg.articleid*(-1)}</c:if><c:if test="${msg.articleid>=0}">${msg.articleid}</c:if>.html">${msg.content}</a>
	        </li>
		</c:forEach>
	</ul>
</article>
