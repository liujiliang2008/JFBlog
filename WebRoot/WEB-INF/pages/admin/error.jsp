<%@ page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<meta charset="UTF-8">
			<title>Error</title>
			<style>
html {
	padding: 50px 10px;
	font-size: 20px;
	line-height: 1.4;
	color: #666;
	background: #F6F6F3;
	-webkit-text-size-adjust: 100%;
	-ms-text-size-adjust: 100%;
}

html,input {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}

body {
	max-width: 500px;
	_width: 500px;
	padding: 30px 20px 50px;
	margin: 0 auto;
	background: #FFF;
}

h1 {
	font-size: 50px;
	text-align: center;
}

h1 span {
	color: #bbb;
}

ul {
	padding: 0 0 0 40px;
}

.container {
	max-width: 380px;
	_width: 380px;
	margin: 0 auto;
}
</style>
	</head>
	<body>
		<div class="container">
			<h1>
				Error
			</h1>
			<p align="center">${error_info}</p>
			<p align="right"><a href="${returnUrl}">返回上一页</a></p>
		</div>
	</body>
</html>
