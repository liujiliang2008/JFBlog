<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<%@include file="/common/common_meta.jsp" %>
	<link href="css/grid.css" rel="stylesheet" type="text/css" />
	<link href="css/icon.css" rel="stylesheet" type="text/css" />
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<link type="text/css" rel="stylesheet" href="tinymce/plugins/upload/plugin.css" />
	<script src="tinymce/tinymce.min.js"></script>
	<script src="tinymce/plugins/upload/plugin.js"></script>
	<script type="text/javascript">
		tinymce.init({
			selector:'#admin_content',
		   	plugins: [
	          "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
	          "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
	          "table contextmenu directionality emoticons template textcolor paste fullpage textcolor sh4tinymce upload"
	        ],
	        toolbar: "undo redo | sh4tinymce | preview | upload", // toolbar list
	        language:'zh_CN',
	    	upload_action: '${ctx}/upload.html'//required
		});
	</script>
</head>

<body id="article_center">
	<header> 
		<img alt="暂无图片" src="<c:if test="${!empty article_admin.picture}">${article_admin.picture}</c:if>" id="admin_img" style="height: 130px; width: 130px;">
		<input type="file" name="artpic" id="artpic" readonly="readonly"/>
		<input type="button" value="配图上传" id="fileUpload"/>
		<form action="admin/publish.html" method="post" id="form1">
			<input type="hidden" name="article.id" value="${article_admin.id}"/>
			<input type="hidden" id="bindid" name="article.tagsid" value="${article_admin.tagsid}"/>
			文章配图：<input type="text" name="article.picture" value="<c:if test="${!empty article_admin.picture}">${article_admin.picture}</c:if>" id="art_hidden" readonly="readonly"/><span>上传后自动赋值，可手动填写，也可</span><input type="button" id="choose_history" value="选择历史"/></br>
			文章标题：<input type="text" style="width: 35%;" name="article.title" id="arttitle" value="${article_admin.title}" placeholder="请输入文章标题"/>
			<select name="article.isdraft">
				<option value="0">正常</option>
				<option value="1" <c:if test="${article_admin.isdraft=='1'}">selected="selected"</c:if>>草稿</option>
			</select></br>
			文章摘要：<textarea name="article.remark" cols="100" rows="5" placeholder="请输入文章摘要">${article_admin.remark}</textarea>
			<input type="button" value="保存" id="admin_publish"/>
				<textarea cols="40" rows="28" name="article.content" id="admin_content">${article_admin.content}</textarea>
			</div>
		</form>
		<div style="display: none;" id="history_dialog">
			<c:forEach items="${uploadlist}" var="ups">
				<img src="${ups}" alt="暂无图片" style="width: 50px;height: 50px;" id="history_img"/>
			</c:forEach>
		</div>
	</header>
	<script src="js/require.js" defer async="true" data-main="js/admin_publish"></script>
</body>
</html>