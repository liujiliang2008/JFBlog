<%@ page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<%@include file="/common/common_meta.jsp" %>
		<link href="css/normalize.css" rel="stylesheet" type="text/css" />
		<link href="css/grid.css" rel="stylesheet" type="text/css" />
		<link href="css/login.css" rel="stylesheet" type="text/css" />
	</head>

	<body class="body-100">
		<div class="typecho-login-wrap">
			<div class="message popup notice" id="check_login" style="position: fixed; top: 0px; display: none;"><ul><li>${error_info}</li></ul></div>
			<div class="message popup error" style="position: fixed; top: 0px; display: none;"><ul><li>用户名或邮箱无效</li></ul></div>
		    <div class="typecho-login">
		        <h1><a href="" style="text-decoration:none; color: #ADADAB;">XX566.COM</a></h1>
		        <form id="form1" method="post" action="admin/loginToIndex.html">
		            <p>
		                <label class="sr-only" for="name">用户名</label>
		                <input type="text" class="text-l w-100" placeholder="请输入用户名和邮箱" onfocus="javascript:if($(this).val()=='请输入用户名和邮箱'){$(this).val('')}" onblur="javascript:if($(this).val()==''){$(this).val('请输入用户名和邮箱')}" style="color:#6d6d6d;" value="请输入用户名和邮箱" name="name" id="name">
		            </p>
		            <p>
		                <label class="sr-only" for="password">密码</label>
		                <input type="password" placeholder="请输入邮箱地址" class="text-l w-100" name="password" id="password">
		            </p>
		            <input type="hidden" value="" name="returnUrl" id="returnUrl"/>
		            <p class="submit">
		                <button class="btn-l w-100 primary" type="button">登录</button>
		            </p>
<%--		            <p>--%>
<%--		                <label for="remember"><input type="checkbox" id="remember" value="1" class="checkbox" name="remember"> 记住我</label>--%>
<%--		            </p>--%>
		        </form>
		        <p class="more-link"><a href="">返回首页</a></p>
		    </div>
		</div>
		<script src="js/require.js" defer async="true" data-main="js/admin_login"></script>
	</body>
</html>
