<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<header class="navbar"> 
	<section class="container clearfix">
		<h2 class="navbar-header">
			<a href="detail/0.html" class="nav-brand">xx566.com</a>
		</h2>
		<nav class="navbar-inner clearfix">
		<ul class="navbar-nav">
			<li>
				<a href="" ref="navbar_click" target="_parent" title="首页"> <span class="icon-home"></span> 首页 </a>
			</li>
			<li>
				<a href="admin/toPublish.html" title="发布文章" target="_parent"> <span class="icon-archive"><span> 发布文章 </span> </span> </a>
			</li>
			<li>
				<a href="admin/artList.html" title="文章管理" target="_parent"> <span class="icon-archive"><span> 文章管理 </span> </span> </a>
			</li>
			<li>
				<a href="admin/tagList.html" title="标签管理" target="_parent"> <span class="icon-guestbook"><span> 标签管理 </span> </span> </a>
			</li>
			<li>
				<a href="admin/linkList.html" title="友情链接" target="_parent"> <span class="icon-guestbook"><span> 友情链接 </span> </span> </a>
			</li>
			<li>
				<a href="admin/msgList.html" title="留言" target="_parent"> <span class="icon-guestbook"><span> 留言簿 </span> </span> </a>
			</li>
			<c:if test="${isadmin}">
				<li>
					<a href="admin/about.html" title="关于Realfighter" target="_parent"> <span class="icon-about"><span> 关于Realfighter </span> </span> </a>
				</li>
			</c:if>
		</ul>
		</nav>
	</section>
</header>