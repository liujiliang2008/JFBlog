<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<c:forEach items="${page.list}" var="blogs">
	<article>
	     <h2 class="article-title">
	         <a href="detail/${blogs.id}.html">${blogs.title}</a>
	     </h2>
	     <ul class="article-meta">
	         <li><fmt:formatDate value="${blogs.createtime}" pattern="yyyy/MM/dd" /></li>
	         <li><span>|</span></li>
	         <li><a href="1-${blogs.tagsid}-0-0.html">${blogs.name}</a></li>
	         <li><span>|</span></li>
	         <li>${blogs.clicks} Views</li>
	     	 <li><span>|</span></li>
	     	 <li>author：<a onclick="return false;" style="cursor: pointer;">${blogs.username}</a></li>
	     </ul>
	
		<section class="article-content clearfix article-page">
	      	<a href=""><img src="${blogs.picture}" class="thumbnail"></a>
	        ${blogs.remark}   
		</section>
	</article>
</c:forEach>
<c:if test="${!empty page.list}">
	<ol class="page-navigator">
		<c:if test="${page.num!=page.first&&page.num!=0}">
			<li><a href="1-${index_tagid}-${index_time}<c:if test="${! empty search_key}">-${search_key}</c:if>.html">首页</a></li>
			<li><a href="${page.num-1}-${index_tagid}-${index_time}<c:if test="${! empty search_key}">-${search_key}</c:if>.html">上一页</a></li>
		</c:if>
		<c:forEach var="i" begin="${page.begin}" end="${page.end}">
			<li <c:if test="${page.num==i}">style="font-size: 18px;"</c:if>><a href="${i}-${index_tagid}-${index_time}<c:if test="${! empty search_key}">-${search_key}</c:if>.html">${i}</a></li>
		</c:forEach>
		<c:if test="${page.navCount!=0&&page.num!=page.last}">
			<li><a href="${page.next}-${index_tagid}-${index_time}<c:if test="${! empty search_key}">-${search_key}</c:if>.html">下一页</a></li>
			<li><a href="${page.last}-${index_tagid}-${index_time}<c:if test="${! empty search_key}">-${search_key}</c:if>.html">尾页</a></li>
		</c:if>
	</ol>
</c:if>
<c:if test="${empty page.list}">
	<article>
		<h2 class="article-title">
		    <a href="">暂无记录</a>
		</h2>
		<section class="article-content clearfix article-page">
			<p>很抱歉，暂时没有您想要的信息，您可以<a href="message.html">留言</a>或收藏此地址，我会及时更新。</p>
		</section>
	</article>
</c:if>