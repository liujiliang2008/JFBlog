<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>归档</title>
		<%@include file="/common/common_meta.jsp" %>
		<link href="css/grid.css" rel="stylesheet" type="text/css" />
		<link href="css/icon.css" rel="stylesheet" type="text/css" />
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<link href="css/custom.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<%@include file="/common/header.jsp" %>
		<article class="container content">
			<article class="row">
				<section class="article-list col-tb-8 col-mb-12">
					<section id="breadcrumb">
						<a href="">首页</a><span>&gt;</span>归档
            		</section>
					<article>
						<ul id="archives">
							<c:forEach items="${archives}" var="ass">
								<li>
									<span><fmt:formatDate value="${ass.createtime}" pattern="yyyy-MM-dd" /></span>
									<a href="detail/${ass.id}.html">${ass.title}</a>
								</li>
							</c:forEach>
						</ul>
					</article>
					<%@include file="message_form.jsp" %>
				</section>
				<section class="side-list col-tb-4 col-mb-12">
					<%@include file="latest_blog.jsp" %>
					<%@include file="latest_reply.jsp" %>
					<%@include file="category_tags.jsp" %>
					<%@include file="friendly_links.jsp" %>
				</section>
			</article>
		</article>	
  		<%@include file="/common/footer.jsp" %>
  		<script src="js/require.js" defer async="true" data-main="js/index"></script>
	</body>
</html>
