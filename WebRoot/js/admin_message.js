//js配置
require.config( {
	baseUrl : "",
	paths : {
		"jquery" : "js/jquery-1.8.0",
		"jquery.artDialog" : "js/jquery.artDialog",
		"admin.message" : "xx566/admin.message"
	},
	shim : {
		"jquery.artDialog" : {
			deps : [ "jquery" ],
			exports : "artDialog"
		}
	}
});
//留言管理
require( [ "jquery", "admin.message" ], function($, message) {
	$("a[id^='msgs_user']").click(message.userinfo);
	$("a[id^='msgs_delete']").click(message.remove);
});