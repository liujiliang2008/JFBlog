//js配置
require.config( {
	baseUrl : "",
	paths : {
		"jquery" : "js/jquery-1.8.0",
		"xx566.index" : "xx566/index"
	}
});
//首页搜索
require( [ "jquery", "xx566.index" ], function($, index) {
	//站内搜索
	$("#search_btn").click(index.search);
	//导航分类点击
	$("a[id^='index_cate']").click(index.category);
});