//js配置
require.config( {
	baseUrl : "",
	paths : {
		"jquery" : "js/jquery-1.8.0",
		"xx566.index.message" : "xx566/index.message"
	}
});
//留言功能
require( [ "jquery", "xx566.index.message" ], function($, message) {
	$("#msgpid").val(0);
	//发布留言
	$("#msg_submit").live("click", message.addMsg);
	//跟随留言
	$("a[rel^='follow']").live("click", message.followMsg);
	//取消跟随
	$("a[id^='cancel-comment-reply-link']").live("click", message.cancleMsg);
});