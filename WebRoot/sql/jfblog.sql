/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1:3307
Source Server Version : 50620
Source Host           : localhost:3307
Source Database       : xx566_jfinal

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2014-12-06 16:43:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文章ID，-2归档信息，-1留言信息，0关于',
  `content` longtext COMMENT '文章内容',
  `title` varchar(50) DEFAULT NULL COMMENT '文章标题',
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `clicks` int(11) DEFAULT '0' COMMENT '文章阅读数',
  `tagsid` int(11) DEFAULT '0' COMMENT '所属标签ID',
  `picture` varchar(50) DEFAULT '' COMMENT '文章配图',
  `isdraft` int(2) DEFAULT '0' COMMENT '文章的状态，默认0正式文章，1草稿文章',
  `remark` varchar(500) DEFAULT '' COMMENT '文章的摘要',
  `userid` int(11) DEFAULT '0' COMMENT '用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for friendly
-- ----------------------------
DROP TABLE IF EXISTS `friendly`;
CREATE TABLE `friendly` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) DEFAULT '' COMMENT '友情链接',
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `url` varchar(50) DEFAULT '' COMMENT '友链地址',
  `counts` int(11) DEFAULT '0' COMMENT '点击量',
  `orders` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `userid` int(11) DEFAULT NULL COMMENT '留言回复人ID',
  `content` varchar(500) DEFAULT NULL COMMENT '留言内容',
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '留言时间',
  `pid` int(11) DEFAULT '0' COMMENT '父留言ID',
  `articleid` int(11) DEFAULT '0' COMMENT '文章ID，-2归档信息，-1留言信息，0关于',
  `isshow` int(2) DEFAULT '1' COMMENT '是否通过审核，默认1通过，0待审核',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL COMMENT '标签名',
  `type` varchar(32) DEFAULT NULL COMMENT '类型',
  `counts` int(10) unsigned DEFAULT '0' COMMENT '点击量',
  `orders` int(10) unsigned DEFAULT '0' COMMENT '排序',
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `name` varchar(50) DEFAULT '' COMMENT '用户名',
  `pwd` varchar(50) DEFAULT '' COMMENT '登录密码',
  `state` int(11) DEFAULT '0' COMMENT '用户状态，默认0不可用，1正常',
  `avatar` varchar(50) DEFAULT '' COMMENT '用户头像',
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `userip` varchar(50) DEFAULT '' COMMENT '用户IP',
  `email` varchar(50) DEFAULT '' COMMENT '邮箱地址',
  `website` varchar(50) DEFAULT '' COMMENT '网站地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
