<script type="text/javascript" src="syntaxhighlighter_3.0.83/scripts/shCore.js"></script>
<script type="text/javascript" src="syntaxhighlighter_3.0.83/scripts/shBrushJava.js"></script>
<script type="text/javascript" src="syntaxhighlighter_3.0.83/scripts/shBrushJScript.js"></script>
<script type="text/javascript" src="syntaxhighlighter_3.0.83/scripts/shBrushSql.js"></script>
<script type="text/javascript" src="syntaxhighlighter_3.0.83/scripts/shBrushXml.js"></script>
<link href="syntaxhighlighter_3.0.83/styles/shCore.css" rel="stylesheet" type="text/css"/>
<link href="syntaxhighlighter_3.0.83/styles/shCoreEclipse.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
	SyntaxHighlighter.defaults['gutter'] = false;
	SyntaxHighlighter.defaults['toolbar'] = false;
	SyntaxHighlighter.all();
</script>